import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { ProgressIndicatorService } from 'src/app/services/progress-indicator.service';
import { Movie } from '../../models/movie.model';
import { Router, ActivatedRoute } from '@angular/router';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html'
})
export class CatalogComponent implements OnInit, AfterViewInit {
  @ViewChild('searchInput') searchInput;
  private movies = null;
  private genres = []; // our list of unique genres.
  private movieFilter = '';

  constructor(
    private moviesService: MoviesService,
    private progressService: ProgressIndicatorService,
    private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if (this.moviesService.moviesData === null) {
      this.moviesService.onMoviesReady.subscribe(
        (moviesData) => {
          this.setMovies(moviesData);
        }
      );
    } else {
      this.setMovies(this.moviesService.moviesData);
    }
  }

  /**
   * Set debounce for search input and then do the filter at 500ms after typing.
   * This will shrink our movies list and only display the ones matching the filter criteria.
   */
  ngAfterViewInit() {
    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      debounceTime(500),
      distinctUntilChanged()
    ).subscribe((res) => {
      this.progressService.showSpinner();
      this.moviesService.searchMovies(this.movieFilter).subscribe(
        (data) => {
          if (data.movies.length > 0) {
            this.setMovies(data.movies);
          } else {
            this.movies = [];
            this.progressService.hideSpinner();
          }
        },
        (err) => {
          this.progressService.hideSpinner();
          console.log('error: ', err)
        }
      );
    });
  }

  /**
   * Redirects to the detail page.
   * @param movieSelected 
   */
  goTo(movieSelected: Movie) {
    this.router.navigateByUrl('movie-detail/'+movieSelected.slug);
  }

  /**
   * Sets out local copy of movies according to server data response.
   * @param moviesData 
   */
  setMovies(moviesData) {
    // get our own local copy of movies.
    this.movies = moviesData;
    this.movies.forEach((mov: Movie) => {
      // iterate over each movie to get all the genres available.
      mov.genres.forEach(genreId => {
        this.genres.push(genreId); // push each movie genre to our array.
      });
    });
    // create a new array of only unique values & assign it back to our genres array.
    this.genres = Array.from(new Set(this.genres));
    // sort values from low to high just for better iteration.
    this.genres.sort((a, b) => {
      return a - b;
    });
    // our info is ready, so we can stop spinner
    this.progressService.hideSpinner();
  }

  /**
   * Filters & sets the total amount of unique genres in our movie list.
   * @param genreId 
   */
  filteredMovies(genreId) {
    const filteredMovies = [];
    this.movies.forEach(mov => {
      if (mov.genres.includes(genreId)) {
        filteredMovies.push(mov);
      }
    });
    return filteredMovies.sort( (a, b) => {
      const titleA = a.title;
      const titleB = b.title;
      if (titleA < titleB) {
        return -1;
      }
      if (titleA > titleB) {
        return 1;
      }
      return 0;
    });
  }

}
