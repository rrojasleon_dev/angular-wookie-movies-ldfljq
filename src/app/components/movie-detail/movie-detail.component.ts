import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { Movie } from 'src/app/models/movie.model';
import { ProgressIndicatorService } from 'src/app/services/progress-indicator.service';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html'
})
export class MovieDetailComponent implements OnInit {

  private movieGot: Movie = null;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _movieService: MoviesService,
    private _progressService: ProgressIndicatorService) { }

  ngOnInit() { 
    this.movieGot = this._movieService.moviesData.find(
      movie => { 
        return movie.slug === this._activatedRoute.snapshot.params['slug'] 
      }
    );
    this._progressService.hideSpinner();
  }

  /**
   * Sets the background-image style usnig the movie backdrop url.
   * @param backdrop 
   * @returns styles: An array of styles to be applied at our element.
   */
  getStyle(backdrop) {
    const styles = [];
    styles['background-image'] = 'linear-gradient(to bottom, rgba(0, 0, 0, 0.7), rgba(39, 39, 39, 0.65)),url(' + backdrop + ')';
    return styles;
  }

  /**
   * Navigate back tou our home component.
   */
  goTo() {
    this._router.navigate(['']);
  }

}
