import { Component, OnInit, Input, Output } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html'
})
export class MovieComponent implements OnInit {
  @Input() movie: Movie; // Movie got as a property.
  @Output() onMovieClicked: EventEmitter<Movie> = new EventEmitter(); // Our event emitter to communicate with parent.

  constructor() { }

  ngOnInit() {}

  /**
   * Sets the background-image style usnig the movie poster url.
   * @param imageUrl 
   * @returns styles: An array of styles to be applied at our element.
   */
  setBackground(imageUrl) {
    const styles = [];
    styles['background-image'] = 'linear-gradient(to bottom, rgba(32, 32, 33, 0.52), rgba(0, 0, 0, 0.8)),url(' + imageUrl + ')';
    return styles;
  }

  /**
   * Emit an event to our parent component (catalog) can react to this action.
   */
  setMovieActive() {
    this.onMovieClicked.emit(this.movie);
  }

}
