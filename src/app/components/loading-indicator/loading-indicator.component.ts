import { Component, OnInit } from '@angular/core';
import { ProgressIndicatorService } from 'src/app/services/progress-indicator.service';

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html'
})
export class LoadingIndicatorComponent implements OnInit {

  constructor(private progressIndicator: ProgressIndicatorService) { }

  ngOnInit() {}

}
