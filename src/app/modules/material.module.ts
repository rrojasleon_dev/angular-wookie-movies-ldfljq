import { NgModule } from '@angular/core';

import { 
    MatButtonModule, 
    MatProgressSpinnerModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatInputModule
 } from '@angular/material';
@NgModule({
    imports: [ 
        MatButtonModule, 
        MatProgressSpinnerModule,
        MatToolbarModule,
        MatCardModule,
        MatIconModule,
        MatInputModule
    ],
    exports: [ 
        MatButtonModule, 
        MatProgressSpinnerModule,
        MatToolbarModule,
        MatCardModule,
        MatIconModule,
        MatInputModule
    ]
})
export class MaterialModule {};

