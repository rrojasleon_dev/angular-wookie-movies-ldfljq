import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalogComponent } from '../components/catalog/catalog.component';
import { MovieDetailComponent } from '../components/movie-detail/movie-detail.component';
import { MovieGuard } from '../services/moviesGuard.service';
import { NotFoundComponent } from '../components/not-found/not-found.component';

const routes = [
    {
        path: '',
        component: CatalogComponent
    },
    {
        path: 'movie-detail/:slug',
        component: MovieDetailComponent,
        canActivate: [MovieGuard]
    },
    {
        path: 'not-found',
        component: NotFoundComponent
    },
    {
        path: '**',
        redirectTo: 'not-found'
    }
];
@NgModule({
    imports: [RouterModule.forRoot(routes, {
        useHash: true,
        onSameUrlNavigation: 'reload'
    })],
    exports: [ RouterModule ]
})
export class MoviesRoutingModule {};