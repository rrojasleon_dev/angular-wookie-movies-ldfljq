// modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
// services
import { RESTService } from '../services/rest.service';
import { LoadingIndicatorComponent } from '../components/loading-indicator/loading-indicator.component';
import { MovieGuard } from '../services/moviesGuard.service';
// components
import { MovieComponent } from '../components/movie/movie.component';
import { CatalogComponent } from '../components/catalog/catalog.component';
import { NotFoundComponent } from '../components/not-found/not-found.component';
// pipes
import { TruncatePipe } from '../pipes/TruncatePipe.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        CatalogComponent,
        MovieComponent,
        LoadingIndicatorComponent,
        NotFoundComponent,
        TruncatePipe
    ],
    imports: [ 
        MaterialModule, 
        CommonModule,
        FormsModule,
        RouterModule
    ],
    providers: [ 
        RESTService,
        MovieGuard
    ],
    entryComponents: [
        LoadingIndicatorComponent
    ],
    exports: [ 
        MaterialModule,
        MovieComponent,
        LoadingIndicatorComponent,
        NotFoundComponent
    ]
})
export class MoviesCommonModule {};