// modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MoviesCommonModule } from './modules/common.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// services
import { ProgressIndicatorService } from './services/progress-indicator.service';
import { MoviesService } from './services/movies.service';
// components
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MoviesRoutingModule } from './modules/routes.module';
import { MovieDetailComponent } from './components/movie-detail/movie-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    MovieDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MoviesCommonModule,
    BrowserAnimationsModule,
    MoviesRoutingModule
  ],
  providers: [ 
    ProgressIndicatorService,
    MoviesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
