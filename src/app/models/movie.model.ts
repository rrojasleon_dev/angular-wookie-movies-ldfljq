export interface Movie {
    backdrop: string,
    classification: string,
    genres: Array<Number>,
    id: string,
    imdb_rating: Number,
    overview: string,
    poster: string,
    released_on: string,
    slug: string,
    title: string
};