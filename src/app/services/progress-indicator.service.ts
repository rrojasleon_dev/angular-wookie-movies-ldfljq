import { Injectable } from '@angular/core';

@Injectable()
export class ProgressIndicatorService {
    private _isSpinnerVisible: boolean = false;
    
    public get isSpinnerVisible() {
        return this._isSpinnerVisible;
    }

    constructor() {}

    showSpinner() {
        this._isSpinnerVisible = true;
    }
    
    hideSpinner() {
        this._isSpinnerVisible = false;
    }

    reset() {
        this._isSpinnerVisible = false;
    }
}