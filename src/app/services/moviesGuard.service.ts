import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MoviesService } from './movies.service';
import { Observable } from 'rxjs';

@Injectable()
export class MovieGuard implements CanActivate {
    constructor(
        private _moviesService: MoviesService, 
        private _router: Router) {}

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (this._moviesService.moviesData !== null) {
            return true;
        }
        // navigate to home page
        this._router.navigate(['']);
        return false;
    }
}