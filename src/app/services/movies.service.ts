import { Injectable, EventEmitter } from "@angular/core";
import { tap } from 'rxjs/operators';
import { RESTService } from './rest.service';
import { Movie } from '../models/movie.model';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class MoviesService {
    private _moviesData: Movie[] = null;
    private _endpoint = 'https://wookie.codesubmit.io/movies';
    private _auth = 'Bearer Wookie2019';
    public onMoviesReady: EventEmitter<any> = new EventEmitter();
    public get moviesData() {
        return this._moviesData;
    }

    constructor(protected restService: RESTService) {
        this.getMovies().subscribe(
            (data: any) => {
                this._moviesData = data.movies;
            },
            (err) => { console.log('request error: ', err) },
            () => {
                this.onMoviesReady.emit(this._moviesData);
            }
        );
    }

    /**
     * Get all the available movies from server
     * @returns an array of all the movies fetched from a server.
     */
    getMovies() {
        const requestHeaders = new HttpHeaders({
            'Authorization': this._auth
        });

        return this.restService.get(this._endpoint, requestHeaders).pipe(
            tap((response: any) => {
                return response.movies;
            })
        );
    }

    /**
     * Request a search to the server with a specific value.
     * @param searchCriteria 
     * @returns an array of all movies found from server taht applies search criteria.
     */
    searchMovies(searchCriteria) {
        const requestHeaders = new HttpHeaders({
            'Authorization': this._auth
        });

        return this.restService.get(this._endpoint + '?q=' + searchCriteria, requestHeaders).pipe(
            tap((response: any) => {
                return response.movies;
            })
        );
    }
}