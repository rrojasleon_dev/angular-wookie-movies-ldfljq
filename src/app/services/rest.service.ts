import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RESTService {
    constructor(private http: HttpClient) {}

    /**
     * Do a get request to a server.
     * @param endpoint 
     * @param headers 
     * @param params 
     */
    get(endpoint, headers?, params?) {
        if (typeof headers !== 'undefined') {
            if (typeof params !== 'undefined') {
                return this.http.get(endpoint, {
                    headers: headers,
                    params: params
                });
            }
            return this.http.get(endpoint, {
                headers: headers
            });
        }
        return this.http.get(endpoint);
    }
}