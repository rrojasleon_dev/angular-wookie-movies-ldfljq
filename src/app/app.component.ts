import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { ProgressIndicatorService } from './services/progress-indicator.service';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(
    private router: Router,
    private progressIndicator: ProgressIndicatorService
    ) {
      this.router.events.pipe(
        filter(event => event instanceof NavigationStart)
      ).subscribe(
        (event: NavigationStart) => {
          progressIndicator.showSpinner();
        }
      );
  }
}
